#!/usr/bin/env python3


import numpy as np
import pandas as pd
import sqlite3
import io
import gzip
import argparse
import sys
import os
import os.path
import fnmatch


# Function that opens the sqlite database file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db file name, indexes list
# out: sqlite db connection

def open_sqlite(filename, indexLst):
	outMsg= ""
	try:
		#Open the database file
		dbConn = sqlite3.connect(filename)
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
			return {}, outMsg
		
	#Remove the indexes to speed up the insert process
	dbCursor= dbConn.cursor()
	for indexName, null, null in indexLst:
		print("Droping idx "+indexName)
		try:
			dbCursor.execute("DROP INDEX IF EXISTS "+indexName)
		except Exception as E:
			outMsg= outMsg+str(E)+"\n"
	dbConn.commit()
	dbCursor.close()
	return dbConn, outMsg

##

# Function that closes the sqlite database file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db file name, list with index records to create in the format [(indexName, tableName, fieldName)]
# out: sqlite db connection


def close_sqlite(dbConn, indexLst):
	#create the indexes before the close statement
	outMsg= ""
	dbCursor= dbConn.cursor()
	for indexName, tableName, fieldName in indexLst:
		try:
			#Index date_time_idx on field date_time of table compute_collectl
			print("Creating idx "+indexName)
			dbCursor.execute("CREATE INDEX IF NOT EXISTS "+ indexName +" ON "+ tableName +"("+ fieldName +") ")
		except Exception as E:
			outMsg= outMsg+str(E)+"\n"	
	try:
		#Close the database file
		dbConn.commit()
		dbCursor.close()
		dbConn.close()
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
	return outMsg

##


# Function that write a python list into a sqlite table
# creation date: 2020-10-29
# Author: Andre R. Carneiro
# in: sqlite db connection, table name to insert into, input python list, numpy record type
# out: operation status


def write_sqlite_dataset(db,tableName,inputLst,colNames):
	try:
		dataset= pd.DataFrame(inputLst, columns=colNames)
		dataset.to_sql(tableName, db, index=False, if_exists="append")
	except Exception as E:
		return E
	return {}

##



# Function that process all ost files from starting at a source path
# creation date: 2021-04-29
# Author: Andre R. Carneiro
# in: sqlite db, searchPath, date list (format [YYYYMMDD])
# out: operation status


def process_ost_data(searchPath, dbConn, dateLst):
	
	colNames= ['date_time', 'ost', 'reads', 'readkb', 'writes', 'writekb']
	
	fileLst= []
	#workaround in the case of parsing all files in the given search directory
	if not dateLst:
		dateLst.append("")

	#walk through the list of dates and forms a file list to be processed
	for date in dateLst:
		#walth through the "search path" dir and retrive all "ost" named files
		for dName, sdName, fList in os.walk(searchPath):
			for fileName in fList:
				if fnmatch.fnmatch(fileName, "*ost*"+date+"*OST*"):
					fileLst.append(os.path.join(dName, fileName))
	
	for file in fileLst:
		print("Processing file: "+ file)
		#process the collectl file and return a python list with its contents
		fileContentsLst= open_ost_collectl_gzip_file(file)
		#verify if there was an error while processing the collectl file
		if (not fileContentsLst):
			sys.stderr.write("Error while processing file "+file+"!\n")
			continue
				
		result= write_sqlite_dataset(dbConn, "ost_collectl", fileContentsLst, colNames)
		#verify if there was an error while writing the hdf5 file
		if result:
			sys.stderr.write("Error while writing dataset on sqlite file, with message :"+str(result)+"\n")
	return 0

##


# Function that opens ost collectl gziped file 
# creation date: 2020-10-08
# Author: Andre R. Carneiro
# in: collectl gziped file file name
# file input structure
# date time 1st_ost reads readkb writes writekb 2st_ost reads readkb writes writekb ...
#
#
# out: python list to insert into the HDF5 file
# list structure date_time (YYYY-MM-DD HH:MM:00), ost, reads, readkb, write, writekb


def open_ost_collectl_gzip_file(filename):
	#list with the file records
	inputFileLst= []
	
	try:
		for line_aux in io.BufferedReader(gzip.open(filename, "rb")):
			line= line_aux.decode("utf-8").strip()
			if line.startswith('#'):
				continue
			fileFields= line.split()
			#verify if the line contains all the fields
			numberFields= len(fileFields)
			if (numberFields-2)%5 != 0:
				sys.stderr.write("Wrong number of fields at file "+filename+"\n")
				continue
			#concatenate the date and time (fileFields[0] and fileFields[1], respectively) in a single string with format YYYY-MM-DD HH:MM:00
			dateTime= fileFields[0][0:4]+"-"+fileFields[0][4:6]+"-"+fileFields[0][6:8]+" "+fileFields[1][0:6]+"00"
			for i in range(int((numberFields-2)/5)):
				#aux index
				aux=(i*5)+1
				
				#get the ost number
				ost= fileFields[aux+1][-2:]
				
				#get the number read operations
				reads= float(fileFields[aux+2])
				
				#get the volume of the reads (readkb)
				readkb= float(fileFields[aux+3])
				
				if readkb != 0 and reads == 0:
					reads= 0.067
				
				
				#get the number write operations
				writes= float(fileFields[aux+4])
				
				#get the volume of the writes (writekb)
				writekb= float(fileFields[aux+5])
				
				if writekb != 0 and writes == 0:
					writes= 0.067
				
				
				#add file record to the list
				inputFileLst.append((dateTime, ost, reads, readkb, writes, writekb))
	except:
			return {}
	return inputFileLst

##

# Function that process all mdt files from starting at a source path
# creation date: 2020-10-12
# Author: Andre R. Carneiro
# in: searchPath, sqlite databese connection, list of dates
# out: operation status


def process_mdt_data(searchPath, dbConn, dateLst):
	
	colNames= ['date_time', 'get_attr', 'get_attr_lock', 'stat_fs', 'sync', 'get_xattr', 'set_xattr', 'connect', 'disconnect', 'reint', 'create', 'link', 'set_attr', 'rename', 'unlink', 'file_create']
	
	fileLst= []
	#workaround in the case of parsing all files in the given search directory
	if not dateLst:
		dateLst.append("")

	#walk through the list of dates and forms a file list to be processed
	for date in dateLst:
		#walth through the "search path" dir and retrive all "ost" named files
		for dName, sdName, fList in os.walk(searchPath):
			for fileName in fList:
				if fnmatch.fnmatch(fileName, "*mdt*"+date+"*"):
					fileLst.append(os.path.join(dName, fileName))
	
	for file in fileLst:
		print("Processing file: "+ file)
		#process the collectl file and return a python list with its contents
		fileContentsLst= open_mdt_collectl_gzip_file(file)
		
		#verify if there was an error while processing the collectl file
		if (not fileContentsLst):
			continue
				
		result= write_sqlite_dataset(dbConn, "mdt_collectl", fileContentsLst, colNames)
		#verify if there was an error while writing the hdf5 file
		if result:
			sys.stderr.write("Error while writing dataset on sqlite file, with message :"+str(result)+"\n")
	return 0

##

# Function that opens mdt collectl gziped file 
# creation date: 2020-10-08
# Author: Andre R. Carneiro
# in: mdt collectl gziped file file name
# file input structure
# date time get_attr get_attr_lock stat_fs sync get_xattr set_xattr connect disconnect reint create link set_attr rename unlink file_create
#
# out: python list to insert into the sqlite database file
# list structure date_time (YYYY-MM-DD HH:MM:00), getattr getattr_lock ttatfs sync getxattr setxattr connect disconnect reint create link setattr rename unlink file_create
#

def open_mdt_collectl_gzip_file(filename):
	#list with the file records
	inputFileLst= []
	
	try:
		for line_aux in io.BufferedReader(gzip.open(filename, "rb")):
			line= line_aux.decode("utf-8").strip()
			if line.startswith('#'):
				continue
			
			aux= len(line.split())
			if aux != 2 and aux != 17:
				print(aux+" - "+filename)
				
			#try to read all the fields from the line. If there's an exception, then the MDT is active on the other node and skip to the next line
			try:
				date, time, get_attr, get_attr_lock, stat_fs, sync, get_xattr, set_xattr, connect, disconnect, reint, create, link, set_attr, rename, unlink, file_create = line.split()
			except:
				continue

			#concatenate the date and time (fileFields[0] and fileFields[1], respectively) in a single string with format YYYY-MM-DD HH:MM:00
			dateTime= date[0:4]+"-"+date[4:6]+"-"+date[6:8]+" "+time[0:6]+"00"
				
			#add file record to the list
			inputFileLst.append((dateTime, int(get_attr), int(get_attr_lock), int(stat_fs), int(sync), int(get_xattr), int(set_xattr), int(connect), int(disconnect), int(reint), int(create), int(link), int(set_attr), int(rename), int(unlink), int(file_create)))
	except:
			return {}
	return inputFileLst

##

def main():

	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--search_path', "-s", required=True, metavar='collectl/output/cstor/2020', type=str, help='specify to where to look for the cstor collectl files.')
	parser.add_argument('--date', "-d", required=False, metavar='YYYYMMDD', default='', type=str, help='Specify the date, in the format YYYYMMDD, to collect the files. If not specified, will collect ALL the files found on the search path.')
	parser.add_argument('--plus_days', "-p", required=False, metavar='N', type=int, help='Specify the number of additional days to collect the files, based on the -d option. Ex: -d YYYYMMDD -p 2 will collect the files from dates YYYYMMDD, YYYYMMDD+1Day, YYYYMMDD+2Days')
	parser.add_argument('--file', "-f", required=True, metavar='FILE', type=str, help='Specify file name to use for the SQLite output file. If the doens\'t exist, will create it.')
	parser.add_argument('--target', "-t", required=False, metavar='T', default='b', choices=['b', 'o', 'm'], type=str, help='Specify the target type to parse the files. o -> OST files | m -> MDT files | b -> both (default)')
	

	args = parser.parse_args()
	
	#form the list of dates
	dateLst= []
	if (args.date):
		for dti in pd.period_range(args.date, periods=args.plus_days+1, freq='D'):
			dateLst.append(str(dti).replace('-', ''))
	
	#compute node index list 
	#format [(indexName, tableName, fieldName)]
	indexLst= [('ost_date_time_idx','ost_collectl','date_time'),
		('ost_name_idx','ost_collectl','ost'),
		('mdt_date_time_idx','mdt_collectl','date_time')]

	#Verify if search path exists
	if (not os.path.isdir(args.search_path)):
		sys.stderr.write("Search path "+args.search_path+" isn't a directory. Exiting!\n")
		return 0

	dbOutput, outMsg= open_sqlite(args.file, indexLst)

	#Verify if was able to open the sqlite database file
	if (not dbOutput):
		sys.stderr.write("Error while opening sqlite databae file "+args.file+", with message: "+outMsg+"\n")
		return 0
	else:
		if (outMsg):
			sys.stderr.write("SQlite database file "+args.file+" was open, but with the following message: "+outMsg+"\n")
		
	
	#verify which target to process (only ost, only mdt or both)
	if args.target == 'b':
		print("Processing all OST and MDT files from PATH: "+args.search_path)
		#process ost file list
		process_ost_data(args.search_path, dbOutput, dateLst)
		
		#process mdt file list
		process_mdt_data(args.search_path, dbOutput, dateLst)
		
	elif args.target == 'o':
		print("Processing all OST files from PATH: "+args.search_path)
		#process ost file list
		process_ost_data(args.search_path, dbOutput, dateLst)
		
	elif args.target == 'm':
		print("Processing all MDT files from PATH: "+args.search_path)
		#process mdt file list
		process_mdt_data(args.search_path, dbOutput, dateLst)
	
	outMsg= close_sqlite(dbOutput, indexLst)
	if outMsg:
		sys.stderr.write("Error while closing sqlite databae file "+args.file+", with message: "+outMsg+"\n")
	return 0

if __name__ == "__main__":
	main()
