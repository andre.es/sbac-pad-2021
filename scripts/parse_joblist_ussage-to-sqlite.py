#!/usr/bin/env python3


import pandas as pd
import numpy as np
import sqlite3
import argparse
import sys

# Function that opens the sqlite database file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db file name, indexes list
# out: sqlite db connection

def open_sqlite(filename, indexLst):
	outMsg= ""
	try:
		#Open the database file
		dbConn = sqlite3.connect(filename)
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
			return {}, outMsg
		
	#Remove the indexes to speed up the insert process
	dbCursor= dbConn.cursor()
	for indexName, null, null in indexLst:
		print("Droping idx "+indexName)
		try:
			dbCursor.execute("DROP INDEX IF EXISTS "+indexName)
		except Exception as E:
			outMsg= outMsg+str(E)+"\n"
	dbConn.commit()
	dbCursor.close()
	return dbConn, outMsg

##

# Function that closes the sqlite database file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db file name, list with index records to create in the format [(indexName, tableName, fieldName)]
# out: sqlite db connection


def close_sqlite(dbConn, indexLst):
	#create the indexes before the close statement
	outMsg= ""
	dbCursor= dbConn.cursor()
	for indexName, tableName, fieldName in indexLst:
		try:
			#Index date_time_idx on field date_time of table compute_collectl
			print("Creating idx "+indexName)
			dbCursor.execute("CREATE INDEX IF NOT EXISTS "+ indexName +" ON "+ tableName +"("+ fieldName +") ")
		except Exception as E:
			outMsg= outMsg+str(E)+"\n"	
	try:
		#Close the database file
		dbConn.commit()
		dbCursor.close()
		dbConn.close()
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
	return outMsg

##



# Function that opens the joblist file
# creation date: 2020-11-01
# Author: Andre R. Carneiro
# in: joblist csv file
# out: pandas dataframe

def open_joblist_csv(filename):
	outMsg= ""
	try:
		#Open the database file
		outDF = pd.read_csv(filename, delimiter="|")
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
			return pd.DataFrame(), outMsg
	
	return outDF, outMsg

##




# Function that write a python list into a sqlite table
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db connection, table name to insert into, input python list, list of colum names
# out: operation status

def write_sqlite_dataset(db,tableName, inputLst, colNames):
	try:
		dataset= pd.DataFrame(inputLst, columns=colNames )
		dataset.to_sql(tableName, db, index=False, if_exists="append")
	except Exception as E:
		return E
	return {}


##


# Function that cross joblist data with compute node lustre usage, based on start date and end date
# creation date: 2020-11-01
# Author: Andre R. Carneiro
# in: sqlite compute node db, pandas DF with joblist csv content, output sqlite db, start date, end date
# out: operation status


def cross_joblist_compute(dbCompute, joblistDf, dbOutput, startDate, endDate):
	#colNames= ['date_time', 'node_name', 'reads', 'readkb', 'writes', 'writekb', 'file_open', 'file_close', 'get_attr', 'set_attr', 'seek', 'fsync', 'drt_hit', 'drt_mis']

	#walkthrough the sliced pandas df and select the records from the compute
	#	node database based on job's nodeList and start / end time 
	#Then, write the output on the output database
	##	previously we had 2 tables:
	##		1 - with the summary of the job
	##		2 - with the time series of the job's usage
	for dfIdx, dfRow in joblistDf.iterrows(): 
		print(dfRow["JobID"], dfRow["Start"], dfRow["End"], dfRow["NodeList"])

		#get a python list from the nodeList
		nodes= dfRow["NodeList"].split(',')

		#form the sql query based on the start, end and nodeList
		selectQuery = f"SELECT * from compute_ost_collectl where date_time >= ? and date_time <= ? and node_name in ({','.join(['?']*len(nodes))})"
		print(selectQuery)
		print("querying compute database")
		outDf=  pd.read_sql_query(selectQuery, dbCompute, params=([dfRow["Start"], dfRow["End"]] + nodes))
		print(outDf)
		outDf.insert(1, 'jobid', dfRow["JobID"])
		outDf.insert(2, 'account', dfRow["Account"])
		outDf.insert(3, 'ka', dfRow["KA"])
		outDf.insert(4, 'application', dfRow["Exec"])
		print("inserting in the joblist database\n\n")
		outDf.to_sql("job_usage", dbOutput, index=False, if_exists="append")
		print(outDf)

	

	#for quick lookup - from old code - should be removed in the future
# 	for dName, sdName, fList in os.walk(searchPath):
# 		for fileName in fList:
# 			if fnmatch.fnmatch(fileName, "*mdt*"+startDate+"*"):
# 				fullFileName=os.path.join(dName, fileName)
# 				nodeName=fileName.split("-")[1]
# 					
# 				#process the collectl file and return a python list with its contents
# 				fileContentsLst= open_compute_collectl_gzip_file(fullFileName, nodeName)
# 				
# 				#verify if there was an error while processing the collectl file
# 				if (not fileContentsLst):
# 					continue
# 				
# 				result= write_sqlite_dataset(dbConn, "compute_collectl", fileContentsLst, colNames)
# 				#verify if there was an error while writing the hdf5 file
# 				if result:
# 					sys.stderr.write("Error while writing dataset on sqlite databese file - with error: "+str(result)+"\n")
	return 0

##



def main():

	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--start_date', "-s", required=True, metavar='YYYYMMDD', default='', type=str, help='Specify the start date, in the format YYYYMMDD, to collect cross reference jobs x compute nodes collectl data.')
	parser.add_argument('--end_date', "-e", required=True, metavar='YYYYMMDD', default='', type=str, help='Specify the end date, in the format YYYYMMDD, to collect cross reference jobs x compute nodes collectl data.')
	parser.add_argument('--file', "-f", required=True, metavar='FILE', type=str, help='Specify file name to use for the SQLite database job usage output file. If the doens\'t exist, will create it.')
	parser.add_argument('--compute_file', "-c", required=True, metavar='FILE', type=str, help='Specify file name to use for the SQLite compute node database file.')
	parser.add_argument('--joblist_file', "-j", required=True, metavar='FILE', type=str, help='Specify file name to use for the joblist')


	args = parser.parse_args()
	
	startDate= args.start_date[0:4]+"-"+args.start_date[4:6]+"-"+args.start_date[6:8]+" 00:00:00"
	endDate= args.end_date[0:4]+"-"+args.end_date[4:6]+"-"+args.end_date[6:8]+" 23:59:59"

	dbOutput, outMsg= open_sqlite(args.file, [])

	#Verify if was able to open the sqlite database file
	if (not dbOutput):
		sys.stderr.write("Error while opening sqlite databae file "+args.file+", with message: "+outMsg+"\n")
		return 0
	else:
		if (outMsg):
			sys.stderr.write("SQlite database file "+args.file+" was open, but with the following message: "+outMsg+"\n")
	
	dbCompute, outMsg= open_sqlite(args.compute_file, [])

	#Verify if was able to open the sqlite database file
	if (not dbCompute):
		sys.stderr.write("Error while opening sqlite databae file "+args.file+", with message: "+outMsg+"\n")
		return 0
	else:
		if (outMsg):
			sys.stderr.write("SQlite database file "+args.file+" was open, but with the following message: "+outMsg+"\n")
	
	#Open the joblist csv file
	joblistDf, outMsg= open_joblist_csv(args.joblist_file)
	if (joblistDf.empty):
		sys.stderr.write("Error while opening sqlite database file "+args.joblist_file+", with message: "+outMsg+"\n")
		outMsg= close_sqlite(dbOutput, [])
		if outMsg:
			sys.stderr.write("Error while closing sqlite database file "+args.file+", with message: "+outMsg+"\n")
		outMsg= close_sqlite(dbCompute, [])
		if outMsg:
			sys.stderr.write("Error while closing sqlite database file "+args.compute_file+", with message: "+outMsg+"\n")
		return 0
	
	print("Crossing joblist with compute node collectl data from dates: "+ startDate +" - "+ endDate)
	
	#slice the job list dataframe based on the start and end dates
	#date_slice= np.logical_and(joblistDf['Start'] >= startDate, joblistDf['End'] <= endDate)
	date_slice= np.logical_and(joblistDf['Start'] < endDate, joblistDf['End'] >= startDate)
	#process data
	cross_joblist_compute(dbCompute, joblistDf[date_slice], dbOutput, args.start_date, args.end_date)
	
	outMsg= close_sqlite(dbCompute, [])
	if outMsg:
		sys.stderr.write("Error while closing sqlite database file "+args.compute_file+", with message: "+outMsg+"\n")
	
	#compute node index list 
	#format [(indexName, tableName, fieldName)]
	indexLst= [('date_time_idx','job_usage','date_time')]
	outMsg= close_sqlite(dbOutput, indexLst)
	if outMsg:
		sys.stderr.write("Error while closing sqlite database file "+args.file+", with message: "+outMsg+"\n")
		
	return 0	
	

if __name__ == "__main__":
	main()

