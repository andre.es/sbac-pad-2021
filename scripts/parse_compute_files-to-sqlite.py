#!/usr/bin/env python3


import pandas as pd
import numpy as np
import sqlite3
import io
import gzip
import argparse
import sys
import os
import os.path
import fnmatch
import multiprocessing as mp
from datetime import datetime



# Function that opens the sqlite database file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db file name, indexes list
# out: sqlite db connection

def open_sqlite(filename, indexLst):
	outMsg= ""
	try:
		#Open the database file
		dbConn = sqlite3.connect(filename)
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
			return {}, outMsg
		
	#Remove the indexes to speed up the insert process
	dbCursor= dbConn.cursor()
	for indexName, null, null in indexLst:
		print("Droping idx "+indexName)
		try:
			dbCursor.execute("DROP INDEX IF EXISTS "+indexName)
		except Exception as E:
			outMsg= outMsg+str(E)+"\n"
	dbConn.commit()
	dbCursor.close()
	return dbConn, outMsg

##

# Function that closes the sqlite database file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db file name, list with index records to create in the format [(indexName, tableName, fieldName)]
# out: sqlite db connection


def close_sqlite(dbConn, indexLst):
	#create the indexes before the close statement
	outMsg= ""
	dbCursor= dbConn.cursor()
	for indexName, tableName, fieldName in indexLst:
		try:
			#Index date_time_idx on field date_time of table compute_collectl
			print("Creating idx "+indexName)
			dbCursor.execute("CREATE INDEX IF NOT EXISTS "+ indexName +" ON "+ tableName +"("+ fieldName +") ")
		except Exception as E:
			outMsg= outMsg+str(E)+"\n"	
	try:
		#Close the database file
		dbConn.commit()
		dbCursor.close()
		dbConn.close()
	except Exception as E:
			outMsg= outMsg+str(E)+"\n"
	return outMsg

##



# Function that write a python list into a sqlite table
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db connection, table name to insert into, input python list, numpy record type
# out: operation status



def write_sqlite_dataset(db,tableName,inputLst, colNames):
	try:
		dataset= pd.DataFrame(inputLst, columns=colNames )
		dataset.to_sql(tableName, db, index=False, if_exists="append")
	except Exception as E:
		return E
	return {}


##



# Function that process all files from starting at a source path
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db, searchPath, list of dates to process (format [YYYYMMDD])
# out: operation status


def process_ost_data(searchPath, dbConn, dateLst):
	#colNames= ['date_time', 'node_name', 'ost', 'reads', 'readkb', 'writes', 'writekb']
	colNames= ['date_time', 'ost', 'node_name', 'reads', 'readkb', 'writes', 'writekb']
	fileLst= []
	#workaround in the case of parsing all files in the given search directory
	if not dateLst:
		dateLst.append("")

	#walk through the list of dates and forms a file list to be processed
	for date in dateLst:
		#walth through the "search path" dir and retrive all "ost" named files
		for dName, sdName, fList in os.walk(searchPath):
			for fileName in fList:
				if fnmatch.fnmatch(fileName, "*ost*"+date+"*"):
					fileLst.append(os.path.join(dName, fileName))
	
	for file in fileLst:
		print("Processing file: "+ file)
		#process the collectl file and return a python list with its contents
		fileContentsLst= open_compute_collectl_ost_gzip_file(file)
				
		#verify if there was an error while processing the collectl file
		if (not fileContentsLst):
			continue
		
		result= write_sqlite_dataset(dbConn, "compute_ost_collectl", fileContentsLst, colNames)
		#verify if there was an error while writing the file
		if result:
			sys.stderr.write("Error while writing dataset on sqlite databese file - with error: "+str(result)+"\n")
				
	return 0

##


# Function that opens compute node ost collectl gziped file 
# creation date: 2020-11-04
# Author: Andre R. Carneiro
# in: client mdt collectl gziped file file name
# file input structure
# Date Time FileSys Ost Reads ReadKB Writes WriteKB
#
#
# out: python list to insert into sqlite database file
# list structure date_time (YYYY-MM-DD HH:MM:00), node_name, reads, readkb, write, writekb
#
#NOTE: this version sums up the data from all ost's 

def open_compute_collectl_ost_gzip_file(filename):
	#get the node name
	nodeName = filename.split("-")[1]
	#list with the file records
	inputFileLst= []
	
	try:
		for line_aux in io.BufferedReader(gzip.open(filename, "rb")):
			line= line_aux.decode("utf-8").strip()
			if line.startswith('#'):
				continue
			
			#verify if the line contains all the fields
			fileFields= line.split()
			#verify if the line contains all the fields
			numberFields= len(fileFields)
			if (numberFields-2)%6 != 0:
				sys.stderr.write("Wrong number of fields at file "+filename+"\n")
				continue
								
			
			#concatenate the date and time (fileFields[0] and fileFields[1], respectively) in a single string with format YYYY-MM-DD HH:MM:00
			seconds= fileFields[1][6:8]

			if (int(seconds) < 15):
				seconds="14"
			elif (int(seconds) >= 15 and int(seconds) < 30):
				seconds="29"
			elif (int(seconds) >= 30 and int(seconds) < 45):
				seconds="44"
			elif (int(seconds) >= 45 and int(seconds) < 60):
				seconds="59"
			dateTime= fileFields[0][0:4]+"-"+fileFields[0][4:6]+"-"+fileFields[0][6:8]+" "+fileFields[1][0:6]+seconds
			
			reads= 0
			readkb= 0
			writes= 0
			writekb= 0
			
			for i in range(int((numberFields-2)/6)):
				#aux index
				aux=(i*6)+2
				
				#get the ost number
				ost= fileFields[aux+1][-2:]
				
				#get the number read operations
				lreads= float(fileFields[aux+2])
				
				#get the volume of the reads (readkb)
				lreadkb= float(fileFields[aux+3])
				
				if lreadkb != 0 and lreads == 0:
					lreads= 0.067
				
				
				#get the number write operations
				lwrites= float(fileFields[aux+4])
				
				#get the volume of the writes (writekb)
				lwritekb= float(fileFields[aux+5])
				
				if lwritekb != 0 and lwrites == 0:
					lwrites= 0.067
								
				#do not sum up the falues from the osts
				inputFileLst.append((dateTime, ost, nodeName, float(lreads), float(lreadkb), float(lwrites), float(lwritekb)))
				
	except:
			return {}
	return inputFileLst

##


# Function that process all files from starting at a source path
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: sqlite db, searchPath, list of dates to process (format [YYYYMMDD])
# out: operation status


def process_mdt_data(searchPath, dbConn, dateLst):
	colNames= ['date_time', 'node_name', 'reads', 'readkb', 'writes', 'writekb', 'file_open', 'file_close', 'get_attr', 'set_attr', 'seek', 'fsync', 'drt_hit', 'drt_mis']
	fileLst= []
	
	if not dateLst:
		dateLst.append("")

	#walk through the list of dates and forms a file list to be processed
	for date in dateLst:
		#walth through the "search path" dir and retrive all "ost" named files
		for dName, sdName, fList in os.walk(searchPath):
			for fileName in fList:
				if fnmatch.fnmatch(fileName, "*mdt*"+date+"*"):
					fileLst.append(os.path.join(dName, fileName))
	
	for file in fileLst:
		print("Processing file: "+ file)
		#process the collectl file and return a python list with its contents
		fileContentsLst= open_compute_collectl_mdt_gzip_file(file)
				
		#verify if there was an error while processing the collectl file
		if (not fileContentsLst):
			continue
		
		result= write_sqlite_dataset(dbConn, "compute_collectl", fileContentsLst, colNames)
		#verify if there was an error while writing the hdf5 file
		if result:
			sys.stderr.write("Error while writing dataset on sqlite databese file - with error: "+str(result)+"\n")
				
	return 0

##

# Function that opens compute node mdt collectl gziped file 
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: client mdt collectl gziped file file name
# file input structure
# Date Time FileSys Reads ReadKB Writes WriteKB Open Close GAttr SAttr Seek Fsync DrtHit DrtMis
#
#
# out: python list to insert into sqlite database file
# list structure date_time (YYYY-MM-DD HH:MM:00), reads, readkb, write, writekb, fopen, fclose, gattr, sattr, seek, fsync, drtHit, drtMis
#
# Important to notice: I'm using the MDT collectl client file because it already 
#	have the number and KB read/written. So, 
#	both filesystem usage and metadata will be on the same dataset
#

def open_compute_collectl_mdt_gzip_file(filename):
	#get the node name
	nodeName = filename.split("-")[1]
	#list with the file records
	inputFileLst= []
	
	try:
		for line_aux in io.BufferedReader(gzip.open(filename, "rb")):
			line= line_aux.decode("utf-8").strip()
			if line.startswith('#'):
				continue
			
			#verify if the line contains all the fields
			fieldNumber= len(line.split())
			
			# sometimes the file comes with different field number
			# the default is 15 fields (Date Time [CLT:cstor]FileSys [CLT:cstor]Reads [CLT:cstor]ReadKB [CLT:cstor]Writes [CLT:cstor]WriteKB [CLTM:cstor]Open [CLTM:cstor]Close [CLTM:cstor]GAttr [CLTM:cstor]SAttr [CLTM:cstor]Seek [CLTM:cstor]Fsync [CLTM:cstor]DrtHit [CLTM:cstor]DrtMis)
			# On other occasions, the line comes with 14 fields (without the FileSys)
			# In Three nodes (sdumont3033, sdumont3193, sdumont5050) and during specific times, the fields are repeated on the line, making it with 28 fields.
			# When there is a problem with the record, the line can come with all kinds of different field number. Those types of records are discarded. 
			if fieldNumber == 15:
				date, time, fileSys, reads, readkb, writes, writekb, fopen, fclose, gattr, sattr, seek, fsync, drtHit, drtMis = line.split()
			elif fieldNumber == 28:
				date, time, null, null, null, null, null, fileSys, reads, readkb, writes, writekb, fopen, fclose, gattr, sattr, seek, fsync, drtHit, drtMis, null, null, null, null, null, null, null, null = line.split()
			elif fieldNumber == 14:
				date, time, reads, readkb, writes, writekb, fopen, fclose, gattr, sattr, seek, fsync, drtHit, drtMis = line.split()
			else:
				continue
								
			
			#concatenate the date and time (fileFields[0] and fileFields[1], respectively) in a single string with format YYYY-MM-DD HH:MM:00
			dateTime= date[0:4]+"-"+date[4:6]+"-"+date[6:8]+" "+time[0:6]+"00"
			
			if float(readkb) != 0 and float(reads) == 0:
				reads= "0.067"
			
			
			if float(writekb) != 0 and float(writes) == 0:
				writes= "0.067"
				
			#add file record to the list
			inputFileLst.append((dateTime, nodeName, float(reads), float(readkb), float(writes), float(writekb), float(fopen), float(fclose), float(gattr), float(sattr), float(seek), float(fsync), float(drtHit), float(drtMis)))
	except:
			return {}
	return inputFileLst

##

def main():

	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--search_path', "-s", required=True, metavar='collectl/output/cstor/2020', type=str, help='specify to where to look for the cstor collectl files.')
	parser.add_argument('--date', "-d", required=False, metavar='YYYYMMDD', default='', type=str, help='Specify the date, in the format YYYYMMDD, to collect the files. If not specified, will collect ALL the files found on the search path.')
	parser.add_argument('--plus_days', "-p", required=False, metavar='N', type=int, default=0, help='Specify the number of additional days to collect the files, based on the -d option. Ex: -d YYYYMMDD -p 2 will collect the files from dates YYYYMMDD, YYYYMMDD+1Day, YYYYMMDD+2Days')
	parser.add_argument('--file', "-f", required=True, metavar='FILE', type=str, help='Specify file name to use for the SQLite output file. If the doens\'t exist, will create it.')
	parser.add_argument('--target', "-t", required=True, metavar='T', choices=['b', 'o', 'm'], type=str, help='Specify the target type to parse the files. o -> OST files | m -> MDT files | b -> both')


	args = parser.parse_args()
	
	#form the list of dates
	dateLst= []
	if (args.date):
		for dti in pd.period_range(args.date, periods=args.plus_days+1, freq='D'):
			dateLst.append(str(dti).replace('-', ''))
		
	#compute node index list 
	#format [(indexName, tableName, fieldName)]
	indexLst= [('date_time_idx','compute_collectl','date_time'),
		('node_name_idx','compute_collectl','node_name'),
		('date_time_idx','compute_ost_collectl','date_time'),
		('node_name_idx','compute_ost_collectl','node_name')]

	#Verify if search path exists
	if (not os.path.isdir(args.search_path)):
		sys.stderr.write("Search path "+args.search_path+" isn't a directory. Exiting!\n")
		return 0
	
	dbOutput, outMsg= open_sqlite(args.file, indexLst)

	#Verify if was able to open the sqlite database file
	if (not dbOutput):
		sys.stderr.write("Error while opening sqlite database file "+args.file+", with message: "+outMsg+"\n")
		return 0
	else:
		if (outMsg):
			sys.stderr.write("SQlite database file "+args.file+" was open, but with the following message: "+outMsg+"\n")
	
	
	
	#verify which target to process (only ost, only mdt or both)
	if args.target == 'b':
		print("Processing all OST and MDT files from PATH: "+args.search_path)
		#process ost file list
		process_ost_data(args.search_path, dbOutput, dateLst)
		
		#process mdt file list
		process_mdt_data(args.search_path, dbOutput, dateLst)
		
	elif args.target == 'o':
		print("Processing all OST files from PATH: "+args.search_path)
		#process ost file list
		process_ost_data(args.search_path, dbOutput, dateLst)
		
	elif args.target == 'm':
		#process file list
		print("Processing MDT files from PATH: "+args.search_path)
		process_mdt_data(args.search_path, dbOutput, dateLst)
	
	
		
	
	outMsg= close_sqlite(dbOutput, indexLst)
	if outMsg:
		sys.stderr.write("Error while closing sqlite database file "+args.file+", with message: "+outMsg+"\n")
	return 0

if __name__ == "__main__":
	main()
