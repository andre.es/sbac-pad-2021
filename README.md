# sbac-pad-2021

Companion repository for the paper **HPC Data Storage at a Glance: The Santos Dumont Experience**

André Ramos Carneiro 1,2 , Jean Luca Bez 1 , Carla Osthoff 2 , Lucas Mello Schnorr 1 , Philippe O. A. Navaux 1

1 Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS) — Porto Alegre, Brazil

2 National Laboratory of Scientific Computation (LNCC) — Petrópolis, Brazil

Contais all data and scripts.
